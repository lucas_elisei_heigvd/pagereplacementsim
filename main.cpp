#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <vector>

#include "PageAlgorithm.h"

using namespace std;

void displayHelp() {
    cout << "Usage: prsim -a <algorithm> -n <page_frames> -s <sequence>" << endl;

    cout << " <algorithm>\tWhich algorithm to simulate. Values: [FIFO, LRU, 2ND]" << endl;
    cout << " <page_frames>\tHow many page frames" << endl;
    cout << " <sequence>\tThe page sequence to compute. Separate each entry by a space" << endl;
}

int main(int argc, char* argv[]) {
    if(argc < 3) {
        displayHelp();

        return EXIT_SUCCESS;
    }

    Algo algo;
    size_t frames = 0;
    vector<int> sequence = vector<int>();

    for(int i = 1; i < argc; ++i) {
        if(!strncmp(argv[i], "-h", 2)) {
            displayHelp();

            return EXIT_SUCCESS;
        } else if(!strncmp(argv[i], "-a", 2)) {
            if(!strcmp(argv[++i], "FIFO")) {
                algo = FIFO;
            } else if(!strcmp(argv[i], "LRU")) {
                algo = LRU;
            } else if(!strcmp(argv[i], "2ND")) {
                algo = SCND;
            } else {
                cout << "Unknown algorithm: " << argv[i] << endl;
                displayHelp();

                return EXIT_FAILURE;
            }
        } else if(!strncmp(argv[i], "-n", 2)) {
            frames = (size_t)atoi(argv[++i]);

            if(frames < 1) {
                cout << "Invalid value for -n: " << argv[i] << ". Must be a positive integer." << endl;

                return EXIT_FAILURE;
            }
        } else if(!strncmp(argv[i], "-s", 2)) {
            while(i + 1 < argc && isdigit(*argv[i + 1])) {
                sequence.push_back(atoi(argv[++i]));
            }
        } else {
            cout << "Unknown option: " << argv[i] << endl;
            displayHelp();

            return EXIT_FAILURE;
        }
    }

    if(sequence.empty()) {
        cout << "Error: You must provide a sequence." << endl;

        return EXIT_FAILURE;
    }

    PageAlgorithm algorithm(algo, frames, sequence);

    return EXIT_SUCCESS;
}

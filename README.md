### Compiling ###
Check that `Makefile` is compliant with your system and in a Terminal window:

```
    cd path/to/folder
    make
```

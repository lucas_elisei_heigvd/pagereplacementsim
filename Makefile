CC = g++
RM = rm

CPP_FLAGS = -std=c++11 -Wall -Wconversion -Wextra -pedantic

TARGETS = prsim

all: $(TARGETS)

prsim: main.cpp
	$(CC) $(CPP_FLAGS) $^ -o $@

clean:
	$(RM) $(TARGETS)

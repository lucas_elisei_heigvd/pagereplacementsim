#ifndef __PAGE_ALGORITHM__
#define __PAGE_ALGORITHM__

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using namespace std;

enum Algo { FIFO, LRU, SCND };

class PageAlgorithm {

// Private fields

private:
    struct Page {
        int n;
        int R;
        bool pointer;
        bool free;
        int lastTime;

        Page(int n, int R = 2, bool pointer = false) : n(n), R(R), pointer(pointer), free(false), lastTime(0) { }
    };

    Algo algo;
    size_t frames;
    vector<bool> faults;
    size_t faultsCount = 0;
    vector<int> sequence;
    vector<vector<Page>> result;

// Public methods

public:
    PageAlgorithm(Algo algo, size_t frames, const vector<int>& sequence) : algo(algo), frames(frames), sequence(sequence) {
        faults = vector<bool>(sequence.size(), false);
        result = vector<vector<Page>>(frames);
        for(size_t i = 0; i < frames; ++i) {
            for(size_t j = 0; j < sequence.size(); ++j)
                result[i].push_back(sequence.at(j));
        }

        compute();
    }

// Private methods

private:
    void compute() {
        switch(algo) {
            case FIFO:
                fifo();
                break;

            case LRU:
                lru();
                break;

            case SCND:
                scnd();
                break;
        }

        printSimulation();
    }

    int contains(const vector<Page> v, int n) {
        for(int i = 0; i < (int)v.size(); ++i) {
            if(v.at(i).n == n)
                return i;
        }

        return -1;
    }

    void fifo() {
        cout << "Launching FIFO simulation..." << endl << endl;

        vector<Page> q = vector<Page>();
        size_t pointer = 0;

        for(size_t i = 0; i < sequence.size(); ++i) {
            int contains = this->contains(q, sequence.at(i));

            if(contains == -1) {
                if(q.size() < frames) {
                    q.push_back(Page(sequence.at(i)));
                } else {
                    q.erase(q.begin() + pointer);
                    q.insert(q.begin() + pointer, Page(sequence.at(i)));
                    pointer = (pointer + 1) % frames;
                }
                faults.at(i) = true;
                ++faultsCount;
            }

            for(size_t j = 0; j < frames; ++j) {
                if(q.size() > j) {
                    result.at(j).at(i) = q.at(j);
                } else {
                    result.at(j).at(i) = Page(0);
                    result.at(j).at(i).free = true;
                }
            }
        }
    }

    void lru() {
        cout << "Launching LRU simulation..." << endl << endl;

        vector<Page> q = vector<Page>();

        for(size_t i = 0; i < sequence.size(); ++i) {
            int contains = this->contains(q, sequence.at(i));

            if(contains == -1) {
                Page page = Page(sequence.at(i), 1);
                page.lastTime = (int)i;

                if(q.size() < frames) {
                    q.push_back(page);
                } else {
                    size_t index = 0;
                    int lastTime = (int)i;

                    for(size_t j = 0; j < q.size(); ++j) {
                        if(q.at(j).lastTime < lastTime) {
                            lastTime = q.at(j).lastTime;
                            index = j;
                        }
                    }

                    q.erase(q.begin() + index);
                    q.insert(q.begin() + index, page);
                }
                faults.at(i) = true;
                ++faultsCount;
            } else {
                q.at(contains).lastTime = (int)i;
                q.at(contains).R = 1;
            }

            for(size_t j = 0; j < frames; ++j) {
                if(q.size() > j) {
                    if(i && result.at(j).at(i - 1).R == 1)
                        q.at(j).R = 2;
                    result.at(j).at(i) = q.at(j);
                } else {
                    result.at(j).at(i) = Page(0);
                    result.at(j).at(i).free = true;
                }
            }
        }
    }

    void printSimulation() {
        cout << "Seq    | ";
        for(int n : sequence)
            cout << n << " | ";
        cout << endl;

        for(size_t i = 0; i < frames; ++i) {
            cout << "Page " << i << " |";
            for(Page page : result.at(i)) {
                if(page.free) {
                    cout << "   |";
                    continue;
                }

                cout << ((page.pointer) ? ">" : " ");
                cout << page.n;
                cout << ((page.R != 0 && page.R != 1) ? " " : ((page.R == 1) ? "+" : "-"));
                cout << "|";
            }
            cout << endl;
        }

        cout << "Faults |";
        for(bool fault : faults) {
            cout << ((fault) ? " X " : "   ");
            cout << "|";
        }
        cout << " ==> " << faultsCount << endl << endl;
    }

    void scnd() {
        cout << "Launching Second Chance simulation..." << endl << endl;

        vector<Page> q = vector<Page>();
        size_t pointer = 0;

        for(size_t i = 0; i < sequence.size(); ++i) {
            int contains = this->contains(q, sequence.at(i));

            Page page = Page(sequence.at(i), 1, true);

            if(contains == -1) {
                if(q.size() < frames) {
                    q.push_back(page);
                } else {
                    while(q.at(pointer).R == 1) {
                        q.at(pointer).R = 0;
                        pointer = (pointer + 1) % frames;
                    }

                    q.erase(q.begin() + pointer);
                    q.insert(q.begin() + pointer, page);

                    pointer = (pointer + 1) % frames;
                }
                faults.at(i) = true;
                ++faultsCount;
            } else {
                q.at(contains).R = 1;
            }

            for(size_t j = 0; j < frames; ++j) {
                if(q.size() > j) {
                    q.at(j).pointer = j == pointer;
                    result.at(j).at(i) = q.at(j);
                } else {
                    result.at(j).at(i) = Page(0);
                    result.at(j).at(i).free = true;
                }
            }
        }
    }

};

#endif  // __PAGE_ALGORITHM__
